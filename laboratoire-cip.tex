%%% Copyright (C) 2019 Vincent Goulet, David Beauchemin
%%%
%%% Ce fichier fait partie du projet
%%% «Initiation au calcul informatique de pointe»
%%% https://gitlab.com/vigou3/laboratoire-cip
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.

\documentclass[x11names]{vgdoc}
  \usepackage{awesomebox}

  \coursenumber{act-2002}
  \coursename{Méthodes numériques en actuariat}

  %% Informations de publication
  \title{Initiation au calcul informatique de pointe \\[6pt]
    {\small\mdseries Version {\fullcaps\year}.\month}}
  \renewcommand{\year}{2019}
  \renewcommand{\month}{01}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/laboratoire-cip}

  %% Hyperlien avec symbole de lien externe juste après; second
  %% argument peut être vide pour afficher l'url comme lien
  %% [https://tex.stackexchange.com/q/53068/24355 pour procédure de
  %% test du second paramètre vide]
  \usepackage{fontawesome}
  \usepackage{relsize}
  \newcommand{\link}[2]{%
    \def\param{#2}%
    \ifx\param\empty
      \href{#1}{\nolinkurl{#1}~\raisebox{-0.1ex}{\smaller\faExternalLink}}%
    \else
      \href{#1}{#2~\raisebox{-0.1ex}{\smaller\faExternalLink}}%
    \fi
  }

  %% Commandes additionnelles
  \newcommand{\code}[1]{\texttt{\upshape #1}}
  \newcommand{\pkg}[1]{\textbf{\upshape #1}}

\begin{document}

\maketitle

\chapter*{Contexte}

\link{https://www.bixi.com/fr}{BIXI~Montréal} est un organisme à but
non lucratif créé en 2014 par la Ville de Montréal pour gérer le
système de vélopartage à Montréal. Le réseau comprend \nombre{6250}
vélos et 540 stations sur le territoire montréalais, ainsi qu’à
Longueuil et Westmount.

Pour le plus grand plaisir des analystes de données, l'organisme rend
disponible ses
\link{https://www.bixi.com/fr/donnees-ouvertes}{volumineuses données}
d'achats et de déplacements. Celles-ci couvrent les années 2014 à 2018
et les huit mois d'activité du service, soit d'avril à novembre.
Seulement pour le mois d'août 2018, on dénombre plus de \nombre{950
  000} enregistrements. Le volume total des données approche le
gigaoctet.

Nous disposons des données du nombre et de la durée des locations, de
même que de la \link{https://www.bixi.com/fr/tarifs}{structure de
  tarification} du service. Il doit bien y avoir moyen de construire
un modèle de prévision des revenus pour 2019 avec tout ça!


\chapter*{Mandat}

Notre mandat pour ce laboratoire consiste à déterminer la distribution
du montant total de revenus pour chacun des mois d'activité du service
BIXI en 2019. Nous souhaitons également obtenir la distribution
agrégée du montant total de revenus pour toute l'année.


\chapter*{Méthodologie}

Nous allons procéder par simulation pour estimer la distribution du
montant total de revenus mensuel et annuel. À cette fin, nous devrons
déterminer la distribution du nombre de locations et la distribution
de la durée de celles-ci pour l'année 2019, et ce, à partir des
données des années 2014 à 2018.

Soit $T$ la variable aléatoire représentant le montant total de
revenus pour un mois donné. En simulant un échantillon aléatoire
$T_1, T_2, \dots, T_n$ de cette variable aléaoire pour une grande
valeur de $n$, nous pourrons obtenir un aperçu de la distribution des
revenus et calculer certaines quantités d'intérêt, comme le revenu
espéré.

Quant à la distribution du montant total de revenus annuel, nous
l'obtiendrons simplement en agrégeant les échantillons aléatoires de
chacun des mois d'activité du service BIXI.


\chapter*{Modélisation}

Familiers que nous sommes avec les modèles fréquence-sévérité, nous
allons utiliser le modèle suivant pour la distribution de la variable
aléatoire $T$ représentant le montant total de revenus mensuel:
\begin{equation}
  \label{eq:T}
  T = R_1 + \dots + R_N,
\end{equation}
où $N$ la variable aléatoire du nombre de locations dans un mois et
$R_j$, est la variable aléatoire des revenus (ou frais, selon le point
de vue) liés à la location $j$, $j = 1, 2, \dots$. Le revenu tiré
d'une location est une fonction directe de la durée de celle-ci. Dans
la suite, nous noterons $Y_j$ la durée de la location $j$.

Sous les hypothèses\footnote{Hypothèses qui ne sont pas satisfaites
  dans le cas présent. Néanmoins, la
  \link{https://en.wikipedia.org/wiki/All_models_are_wrong}{célèbre
    maxime} de George E.~P.~Box s'applique: «Tous les modèles sont
  faux, mais certains sont utiles.»} %
que les variables aléatoires $R_1, R_2, \dots$ sont indépendantes,
identiquement distribuées et mutuellement indépendantes de la variable
aléatoire $N$, et en supposant que la distribution de $N$ est Poisson,
nous savons que la variable aléatoire $T$ est une Poisson composée.

Des âmes charitables --- auxquelles nous référerons dorénavant en tant
qu'«équipe de modélisation» --- se sont chargé de déterminer les
modèles adéquats pour la moyenne de la loi de Poisson et pour la durée
des locations.

\section*{Fréquence des locations}

Le nombre mensuel de locations a connu une forte hausse entre 2014 et
2018. Soit $\mu$ le nombre espéré de locations dans un mois. L'équipe
de modélisation a déterminé que le modèle suivant décrit de manière
acceptable l'évolution de $\mu$ dans le temps:
\begin{equation}
  \label{eq:mu}
  \log \mu = \alpha_0 + \alpha_1 x,
\end{equation}
où $x$ est l'année.

Ainsi, nous avons
\begin{equation}
  \label{eq:N_sachant_x}
  N|x \sim \text{Poisson}(e^{\alpha_0 + \alpha_1 x})
\end{equation}
et nous pouvons estimer les paramètres $\alpha_0$ et
$\alpha_1$ par la méthode du maximum de vraisemblance à partir des
données $(N_{2014}, x_{2014}), \dots, (N_{2018}, x_{2018})$.

\section*{Durée des locations}

Après analyse des données, l'équipe de modélisation est arrivée à
déterminer que la distribution du logarithme des durées de location à
l'intérieur d'un mois est approximativement normale. La moyenne de
cette distribution varie en fonction de l'année, mais la variance
demeure sensiblement la même.

À partir de ces constats, nous pouvons poser:
\begin{equation}
  \label{eq:Y_sachant_x}
  \log(Y_j|x) \sim N(\beta_0 + \beta_1 x, \sigma^2)
\end{equation}
ou, de manière équivalente,
\begin{equation}
  \label{eq:Y_sachant_x_LN}
  Y_j|x \sim \text{Log-normale}(\beta_0 + \beta_1 x, \sigma^2).
\end{equation}
Nous estimons ensuite $\sigma$ à partir de l'ensemble des durées de
location d'un même mois, et les paramètres $\beta_0$ et $\beta_1$ par
la méthode du maximum de vraisemblance à partir des échantillons
$(Y_{2014, 1}, x_{2014}), \dots, (Y_{2018, n}, x_{2018})$. Dans les
deux cas nous disposons de centaines de milliers d'observations.

\section*{Conversion des durées en revenus}

Question de simplifier les choses, nous allons uniquement considérer
les tarifs de location à court terme pour convertir les durées de
location en revenus. De plus, nous modifions légèrement la structure
de tarification pour la faire passer de trois palliers à seulement
deux. Nous utiliserons donc la grille de tarification suivante:
\begin{itemize}
\item tarif de base de 2,95~\$;
\item période de déplacement allouée de 30 minutes;
\item frais supplémentaires de 2,40~\$ par tranche de 15 minutes
  (entamée) après la période de déplacement allouée.
\end{itemize}

Comme nos durées de location sont en secondes, la relation entre les
variables aléatoires $R_j$ et $Y_j$ est:
\begin{equation}
  \label{eq:Rj_vs_Yj}
  R_j = 2,95 + 2,40
  \left\lceil
    \frac{\max(Y_j - \nombre{1800}, 0)}{900}
  \right\rceil,
\end{equation}
où $\lceil x \rceil$ représente le plus petit entier supérieur ou égal
à $x$.

\chapter*{Livrables}



\chapter*{Difficultés numériques}




\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
