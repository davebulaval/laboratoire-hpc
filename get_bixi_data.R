## Copyright (C) 2019 Vincent Goulet, David Beauchemin
##
## Ce fichier fait partie du projet
## «Initiation au calcul informatique de pointe»
## https://gitlab.com/vigou3/laboratoire-cip
##
## Cette création est mise à disposition sous licence
## Attribution-Partage dans les mêmes conditions 4.0
## International de Creative Commons.
## https://creativecommons.org/licenses/by-sa/4.0/

###
### get_bixi_data(year, path = "", clean = TRUE)
###
##  Télécharge l'archive .zip des données ouvertes de BIXI pour une ou
##  plusieurs années.
##
##  Arguments
##
##  year: vecteur d'années à importer.
##  path: répertoire où enregistrer les données (répertoire de travail
##    par défaut).
##  clean: booléen; effacer ou non l'archive téléchargée après avoir
##         extrait son contenu.
##
##  Valeur
##
##  Fichiers de données décompressés dans le répertoire 'path' (sans
##  sous-répertoire).
##
get_bixi_data <- function(year, path = "", clean = TRUE)
{
    baseurl <- "https://www.bixi.com/c/bixi/file_db/data_all.file/"
    root <- "BixiMontrealRentals"

    if (!dir.exists(path))
        dir.create(path, recursive = TRUE)

    for (y in year)
    {
        filename <- paste0(root, y, ".zip")
        url <- paste0(baseurl, filename)
        localfile <- file.path(path, filename)

        if (download.file(url, destfile = localfile))
            stop(paste("error downloading file", url))

        unzip(localfile, junkpaths = TRUE, exdir = path)

        if (clean)
            file.remove(localfile)
    }
}

